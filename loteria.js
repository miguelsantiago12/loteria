$(document).ready(function (){
    function getRandomInteger(min,max) {

        var floatinPoint = Math.random();
        var floatinPointInRange = floatinPoint * (max - min + 1) + min;
        return Math.floor(floatinPointInRange);
    }

    $('.carton__baraja').click(function (){
        $(this).attr('src','baraja/' + getRandomInteger(1, 54) + '.jpg');
    });

    $('.carton__loteria').each(function (){
        $(this).children('img').attr('src','baraja/' + getRandomInteger(1, 54) + '.jpg');
        $(this).children('div').append('<img src="10_centavos.png">');
    });

    $('.carton__loteria > img').click(function (){
        $(this).siblings().show();
    });

    $('.carton__loteria > div').click(function (){
        $(this).hide();
    });
});
